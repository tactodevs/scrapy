import scrapy
import re
import datetime
import codecs
import sys
import json
reload(sys)
sys.setdefaultencoding('utf-8')
from urllib import urlopen
import simplejson as json

class AjioSpider(scrapy.Spider):
    name = "ajio"
    start_urls = [
        'https://www.ajio.com/',
        
    ]

    def parse(self, response):
        categories = {'MEN-SALE':['WESTERN WEAR' , 'FOOTWEAR' , 'ACCESSORIES' , 'INNERWEAR' , 'NIGHT & LOUNGEWEAR'], 'WOMEN-SALE':['WESTERN WEAR' , 'FUSION WEAR' , 'ETHNIC WEAR' , 'FOOTWEAR', 'NIGHT & LOUNGEWEAR', 'LINGERIE & INNERWEAR' , 'BAGS, BELTS & WALLETS' , 'FASHION JEWELLERY']} 
        for category in response.css('div.fnl-header-menu ul li.fnl-cat-height'):
            categoryName = re.sub('\u2022','',''.join(str(e) for e in map(unicode.strip, category.css('div.fnl-header-dropdownv span.headerMenuLnk a::text').extract_first())))
            categoryLink = category.css('div.fnl-header-dropdownv span.headerMenuLnk a::attr(href)').extract_first()
            if (categoryName in categories ):
               for subcategory in category.css('ul.Lb div.fnl-dropdownpagelist div.col-xs-3 div.handpicked-heart a'):
                subcategoryName = subcategory.css('::text').extract_first()
                subcategoryLink = subcategory.css('::attr(href)').extract_first()
                if subcategoryName in categories[categoryName]:
                    #print categoryName+' '+subcategoryName+' '+subcategoryLink
                    yield scrapy.Request("https://www.ajio.com"+subcategoryLink ,method="GET",callback=self.parseListing , meta={'categoryName':categoryName,'subcategoryLink':subcategoryLink ,'subcategoryName':subcategoryName, 'page':2})
                 

    def parseListing(self, response):
        metadata = response.meta
        link = metadata['subcategoryLink']
        data = link.split("/")
        status = 1
        page = 1
        while (status!=0):     
            nextUrl = urlopen('https://www.ajio.com/rilfnlwebservices/v2/rilfnl/products/category/'+str(data[2])+'?query=%3Arelevance&fields=FULL&currentPage='+str(page)+'&pageSize=36&format=json').read()
            urls = json.loads(nextUrl)
            products = urls.get('products')
            if products:
                for product in products:
                    link = "https://www.ajio.com"+product.get('url')
                    yield scrapy.Request(link,callback=self.product,method="GET", meta={'categoryName':metadata['categoryName'], 'link': link , 'subcategoryName':metadata['subcategoryName']})  
                page = page+1
                status = 1
            else:
                status = 0


    def product(self, response):
        meta = response.meta
        for product in response.css('div.fnl-pdp-container'):
            now = datetime.datetime.now()
            if(meta['categoryName'] == 'MEN-SALE'):
                gender = "male"
            if(meta['categoryName'] == 'WOMEN-SALE'):
                gender = "female"
            category = response.css('div.fnl-plp-searchcat ol li a span::text').extract()
            var = product.css('div.variant_options form div div.fnl-pdp-offerPrize span.fnl-pdp-priceStrike span.fnl-cart-finprc-amt::text')
            if var:
                actual_price = re.sub('Rs. ','',''.join(str(e) for e in map(unicode.strip, product.css('div.variant_options form div div.fnl-pdp-offerPrize span.fnl-pdp-priceStrike span.fnl-cart-finprc-amt::text').extract())))
                discounted_price = re.sub('Rs. ','',''.join(str(e) for e in map(unicode.strip, product.css('div.fnl-pdp-prize span.fnl-cart-finprc-amt::text').extract())))
            else:
                actual_price = re.sub('Rs. ','',''.join(str(e) for e in map(unicode.strip, product.css('div.fnl-pdp-prize span.fnl-cart-finprc-amt::text').extract())))
                discounted_price = re.sub('Rs. ','',''.join(str(e) for e in map(unicode.strip, product.css('div.fnl-pdp-prize span.fnl-cart-finprc-amt::text').extract())))      
            scrappedImages = response.css('div.product-thumbnails img::attr(data-primaryimagesrc)').extract()
            images=[]
            if scrappedImages:
                for image in scrappedImages:
                    images.append("https://www.ajio.com"+image)
            yield{
                    'subcategory':category[3],
                    'url':meta['link'],
                    'created_at':now.strftime("%Y-%m-%d %H:%M:%S"),
                    'brand':re.sub('\u2022','',''.join(str(e) for e in map(unicode.strip, product.css('div.fnl-pdp-prodDetails h1.fnl-pdp-title::text').extract_first()))),
                    'name':re.sub('\u2022','',''.join(str(e) for e in map(unicode.strip, product.css('div.fnl-pdp-prodDetails h1.fnl-pdp-title span.fnl-pdp-subtitle::text').extract()))),
                    'description':product.css('ul.fnl-pdp-bullets li h3').extract(),
                    'store_id':"58f21659cae55d0106c7a0c8",
                    'gender':gender,
                    'images':images,
                    'color':re.sub('\u2022','',','.join(str(e) for e in map(unicode.strip, product.css('div.colour div.fnl-pdp-colorCenter div.fnl-pdp-colorName span.fnl-sizecolorspec::text').extract()))),
                    'sizes':map(unicode.strip, product.css('div.sizetestpdp a.pdpsizeVariant div.fnl-pdp-sizeGroup::text').extract()),
                    'sku':re.sub('\u2022','',''.join(str(e) for e in map(unicode.strip, product.css('ul.fnl-pdp-bullets li span#pdetailsCode h3::text').extract()))),
                    'discounted_price':discounted_price,
                    'actual_price':actual_price,
                    'category':meta['subcategoryName']


                    
            }

