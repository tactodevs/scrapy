import scrapy
import re
import codecs
import sys
import json
reload(sys)
import datetime
import os

sys.setdefaultencoding('utf-8')



class ForeverSpider(scrapy.Spider):
    name = "forever"
    start_urls = [
        'http://www.forever21.com/IN/Product/Main.aspx?br=f21',
        
    ]

    def parse(self, response):
        women = ['Dresses' , 'Tops' , 'Sweaters & Knits' , 'Jackets & Coats' , 'Denim' , 'Leggings' , 'Pants' , 'Shorts' , 'Skirts' , 'Rompers & Jumpsuits' , 'Intimates & Sleep' , 'Swimwear' , 'shoes' , 'Accessories']
        men = ['Tees & Tanks' , 'Shirts & Polos' , 'Knits & Cardigans' , 'Sweatshirts & Hoodies' , 'Jackets & Vests' , 'Denim' , 'Pants' , 'Underwear & Socks' , 'Hats & Scarves' , 'Accessories']
        for category in response.css('ul.navigation li'):
            categoryName = category.css('a.dropdown-toggle::text').extract_first()
            categoryLink = category.css('a.dropdown-toggle::attr(href)').extract_first()
            for subcategory in category.css('div.dropdown-menu div.col a'):
                    subcategoryName = subcategory.css('::text').extract_first() 
                    subcategoryLink = subcategory.css('::attr(href)').extract_first()
                    if (categoryName == 'Men'):
                            if subcategoryName in men:
                                yield scrapy.Request(subcategoryLink, callback=self.parseListing, meta={'subcategoryName':subcategoryName, 'categoryName':categoryName,'subcategoryLink':subcategoryLink,'page':2})
                    elif (categoryName == 'Women'):
                            if subcategoryName in women:
                                yield scrapy.Request(subcategoryLink, callback=self.parseListing, meta={'subcategoryName':subcategoryName, 'categoryName':categoryName,'subcategoryLink':subcategoryLink, 'page':2})
                    else:
                            print ""

    def parseListing(self, response):
        metadata = response.meta
        status = 1 if len(response.css('div.ItemImage')) else 0
        if(status):
            for listing in response.css('div.ItemImage'):
                listingLink = listing.css('a::attr(href)').extract_first()
                yield scrapy.Request(listingLink,callback=self.product, meta={'categoryName':metadata['categoryName'],'subcategoryLink':metadata['subcategoryLink'], 'subcategoryName':metadata['subcategoryName'], 'listingLink':listingLink} )
            
            nextUrl = (metadata['subcategoryLink']+'&pagesize=30&page='+str(metadata['page']))
            yield scrapy.Request(nextUrl,callback=self.parseListing,meta={'listingLink':listingLink, 'categoryName':metadata['categoryName'], 'subcategoryName':metadata['subcategoryName'],'subcategoryLink':metadata['subcategoryLink'], 'page':(metadata['page']+1)})  
        else:
            print 'No Data'
    def product(self,response):
        meta = response.meta
        for products in response.css('div#midMainContent'):
            if (meta['categoryName'] == 'Men'):
                gender = "male"
            else:
                gender = "female"

            now = datetime.datetime.now()
            images = response.css('div#scroller_frame ul#scroller li img.ButtonImage::attr(src)').extract()
            images = [w.replace('_58', '_330') for w in images]
            
            yield{
                'url':meta['listingLink'],
                'name': re.sub('\u2022','',''.join(str(e) for e in map(unicode.strip, products.css('div.ProdMargin font.items_name::text').extract()))),
                'gender':gender,
                'store_id':"58f2173acae55dff05c7a0c8",
                'category':meta['categoryName'],
                'subcategory':meta['subcategoryName'],
                'description':re.sub('\u2022','',''.join(str(e) for e in map(unicode.strip, products.css('section.d_wrapper div.d_content span::text').extract()))),
                'sku':re.sub('\u2022','',''.join(str(e) for e in map(unicode.strip, products.css('input#ctl00_MainContent_dlColorChart_ctl01_hdProductSKU::attr(value)').extract()))),
                'actual_price':re.sub('Rs.','',''.join(str(e) for e in map(unicode.strip, products.css('div.ProdMargin font.items_price::text').extract()))),
                'discounted_price':re.sub('Rs.','',''.join(str(e) for e in map(unicode.strip, products.css('div.ProdMargin font.items_price::text').extract()))),
                'images':images,
                'created_at':now.strftime("%Y-%m-%d %H:%M:%S"),
                'color':re.sub('Color,','',','.join(str(e) for e in map(unicode.strip, products.css('div#ctl00_MainContent_upColorList select#ctl00_MainContent_ddlColor option::text').extract()))),

            }   