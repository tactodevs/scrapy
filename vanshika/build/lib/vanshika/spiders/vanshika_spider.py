import scrapy
import re
import codecs
import sys
import json
import datetime
reload(sys)
sys.setdefaultencoding('utf-8')
from scrapy.spiders import CrawlSpider

class VanshikaSpider(scrapy.Spider):
    http_user = 'someuser'
    http_pass = 'somepass'
    name = "vanshika"
    start_urls = [
        'http://vanshikaahuja.com/',
        
    ]

    def parse(self, response):
        category = ['Kohlapuri', 'Clutches', 'Jutti']
        for categories in response.css('div.left-links ul.header-nav div.nav-dropdown ul li.menu-item a'):
            categoryName = categories.css('::text').extract_first().strip()
            categoryLink = categories.css('a::attr(href)').extract_first()
            #print categoryName
            if categoryName in category:
               yield scrapy.Request(categoryLink, self.parse_listing, meta={'categoryName':categoryName})

    def parse_listing(self,response):
        metaData = response.meta
        for listing in response.css('div.style-grid3 table tr td a'):
            listingLink = listing.css('::attr(href)').extract_first()
            #listingName = listing.css('p.name::text').extract()
            yield scrapy.Request(listingLink, self.productDetail, meta={'categoryName':metaData['categoryName'] , 'link':listingLink})


        next_page = response.css('li a.next::attr(href)').extract_first()
        if next_page is not None:
            next_page = response.urljoin(next_page)
            yield scrapy.Request(next_page, callback=self.parse_listing, meta={'categoryName':metaData['categoryName']})
            

    def productDetail(self,response):
        metadata = response.meta
        store = "5979e1c970adb97a1bb72e53"
        for product in response.css('div.site-main'):
            now = datetime.datetime.now()
            size = product.css('table.variations tbody tr td.value select option::attr(value)').extract()
            del size[0]

            if (product.css('span.sku::text').extract_first().strip() == 'N/A'):
                sku = ""
            else:
                sku = re.sub('\u2022','',','.join(str(e) for e in map(unicode.strip, product.css('span.sku::text').extract())))

            if (product.css('p.price del span.woocommerce-Price-amount').extract_first()):
                actual_price = re.sub('\u2022','',''.join(str(e) for e in map(unicode.strip, product.css('p.price del span.woocommerce-Price-amount::text').extract_first())))
                discounted_price = re.sub('\u2022','',''.join(str(e) for e in map(unicode.strip, product.css('meta::attr(content)').extract_first())))
            else:
                actual_price = re.sub('\u2022','',''.join(str(e) for e in map(unicode.strip, product.css('meta::attr(content)').extract_first())))
                discounted_price = re.sub('\u2022','',''.join(str(e) for e in map(unicode.strip, product.css('meta::attr(content)').extract_first())))

            yield{
                    'name':re.sub('\u2022','',''.join(str(e) for e in map(unicode.strip, product.css('h1.entry-title::text').extract()))),
                    'description':re.sub('\u2022','',''.join(str(e) for e in map(unicode.strip, product.css('div.short-description p::text').extract()))),
                    'category':metadata['categoryName'],
                    'actual_price':actual_price,
                    'discounted_price':discounted_price,
                    'sizes':size,
                    'color':re.sub('\u2022','',''.join(str(e) for e in map(unicode.strip, product.css('table.shop_attributes tr.alt td p::text').extract()))),
                    'sku':sku,
                    'tags':re.sub('\u2022','',','.join(str(e) for e in map(unicode.strip, product.css('span.tagged_as a::text').extract()))),
                    'images':response.css('div.easyzoom a::attr(href)').extract(),
                    'url':metadata['link'],
                    'store_id':store,
                    'gender':"female",
                    'created_at':now.strftime("%Y-%m-%d %H:%M:%S")
                }


