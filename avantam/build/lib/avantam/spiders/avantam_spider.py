import scrapy
import re
import codecs
import sys
import json
reload(sys)
import datetime
sys.setdefaultencoding('utf-8')


class AvantamSpider(scrapy.Spider):
    handle_httpstatus_list = [404]
    name = "avantam"
    start_urls = [
        'https://www.aavantam.com/',
        
    ]

    def parse(self, response):
        categories = ['Women' , 'Men']
        for category in response.css('div.main-nav-wrapp ul.main-nav li a'):
            categoryName = category.css('::text').extract_first()
            categoryLink = category.css('::attr(href)').extract_first()
            if categoryName in categories:
                yield scrapy.Request("https://www.aavantam.com"+categoryLink, callback=self.parseListing, meta={'categoryName':categoryName , 'categoryLink':categoryLink, 'page':2})

    def parseListing(self, response):
        metadata = response.meta
        status = 1 if (len(response.css('li.product_list_inner')) and response.status == 200) else 0
        menuId = 0
        if status:
            if (metadata['categoryName'] == 'Women'):
                menuId = 2
            elif (metadata['categoryName'] == 'Men'):
                menuId = 1
            else :
                menuId = 0
            for listing in response.css('li.product_list_inner div.main a'):
                    listingLink = listing.css('::attr(href)').extract_first()
                    if listingLink:
                        #print listingLink
                        yield scrapy.Request("https://www.aavantam.com"+listingLink, callback=self.products, meta={'categoryName':metadata['categoryName'], 'listingLink':listingLink})
            nextUrl = ('https://www.aavantam.com/products/findMoreProducts/0/0/'+str(menuId)+'/'+str(metadata['page'])+'/colour/size/price')
            yield scrapy.Request(nextUrl,callback=self.parseListing,method="GET", meta={'categoryName':metadata['categoryName'],'page':(metadata['page']+1)})
        else:
             print "no data"   

    def products(self, response):
        meta = response.meta
        if(response.status==200):
            for product in response.css('div.prod-details-wrapp div.wrapper'):
                if (meta['categoryName'] == "Women"):
                    gender = "female"
                elif (meta['categoryName'] == "Men"):
                    gender = "male"
                else:
                    gender = "female"
                now = datetime.datetime.now()
                #breadcrumb = map(unicode.strip, product.css('div.page-wrapp a::text').extract())
                yield{
                    'name':re.sub('\u2022','',''.join(str(e) for e in map(unicode.strip, product.css('div.prod-data h1.product_name::text').extract()))),
                    'category':meta['categoryName'],
                    #'subcategory':breadcrumb,
                    'gender':gender,
                    'store_id':"5981844a70adb9814301a70f",
                    'created_at':now.strftime("%Y-%m-%d %H:%M:%S"),
                    'discounted_price':re.sub('INR ','',''.join(str(e) for e in map(unicode.strip, product.css('div.prod-data span.price::text').extract()))),
                    'actual_price':re.sub('INR ','',''.join(str(e) for e in map(unicode.strip, product.css('div.prod-data span.price::text').extract()))),
                    'url':"https://www.aavantam.com"+meta['listingLink'],
                    'description':re.sub('FREE SHIPPING','',''.join(str(e) for e in map(unicode.strip, product.css('div.prod-data ul.quickView-acrd div.colapse p::text').extract()))),
                    'images':map(unicode.strip, product.css('div.pager a span img::attr(src)').extract()),
                    'sizes':map(unicode.strip, product.css('div.prod-data ul.sizeSelect li.size_details a.size::text').extract()),
                    'sku':re.sub('CODE ','',''.join(str(e) for e in map(unicode.strip, product.css('ul.quickView-acrd li div.colapse span.code::text').extract())))
                }
        else:
            print "No Data"