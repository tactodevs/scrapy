import scrapy
import re
import datetime
import codecs
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

class VajorSpider(scrapy.Spider):
    name = "vajor"
    start_urls = [
        'http://www.vajor.com/clothing.html',
        'http://www.vajor.com/shoes.html',
        'http://www.vajor.com/accessories.html',
        'http://www.vajor.com/bags.html',
    ]

    def parse(self, response):
     productLinks = {}
     for category in response.css('div.category-list ul li a'):
        parentCategoryName = category.css('::text').extract_first()
        parentCategoryLink = category.css('::attr(href)').extract_first()
        #print parentCategoryLink
        yield scrapy.Request(parentCategoryLink, self.parse_listing, meta={'parentCategoryName':parentCategoryName})

    def parse_listing(self, response):
        metadata = response.meta
        for product in response.css('a.listimages'):
            productLink = product.css('::attr(href)').extract_first()
            yield scrapy.Request(productLink, self.productDetail, meta={'parentCategoryName':metadata['parentCategoryName'], 'productLink':productLink})  

        next_page = response.css('div.pages a.next::attr(href)').extract_first()
        if next_page is not None:
            next_page = response.urljoin(next_page)
            yield scrapy.Request(next_page, callback=self.parse_listing, meta={'parentCategoryName':metadata['parentCategoryName']})

    def productDetail(self, response):
        metaData = response.meta
        store = "58a54f7087feb1ec113c9869"
        category = map(unicode.strip, response.css('div.one-column ul li a::text').extract())
        for product in response.css('div.product-page'):
            now = datetime.datetime.now()
            var = map(unicode, product.css('div.prod-price div.price-box span.regular-price span.price::text'))
            if var: 
                discounted_price = re.sub('\u2022','',''.join(str(e) for e in map(unicode.strip, product.css('div.prod-price div.price-box span.regular-price span.price::text').extract())))
                actual_price = re.sub('\u2022','',''.join(str(e) for e in map(unicode.strip, product.css('div.prod-price div.price-box span.regular-price span.price::text').extract())))
            
            else:
                discounted_price = re.sub('\u2022','',''.join(str(e) for e in map(unicode.strip, product.css('div.prod-price p.special-price span.price::text').extract())))
                actual_price = re.sub('\u2022','',''.join(str(e) for e in map(unicode.strip, product.css('div.prod-price p.old-price span.price::text').extract())))      
            

            yield {   

                    'name': re.sub('\u2022','',''.join(str(e) for e in map(unicode.strip, product.css('div.prod-sidebar-inner h1::text').extract()))),
                    'images' : map(unicode.strip, response.css('img.originalimage::attr(src)').extract()),
                    'description':re.sub('\u2022','',''.join(str(e) for e in map(unicode.strip, product.css('div.prod-desc::text').extract()))) + re.sub('\u2022','',''.join(str(e) for e in  map(unicode.strip, product.css('div.detailsexp::text').extract()))),
                    'actual_price':actual_price,
                    'discounted_price':discounted_price,
                    'store_id':store,
                    'url':metaData['productLink'],
                    'category':category[1],
                    'subcategory':metaData['parentCategoryName'],
                    'gender':"female",
                    'created_at':now.strftime("%Y-%m-%d %H:%M:%S")
                    }
            
            