import scrapy
import re
import codecs
import datetime
import sys
import json
reload(sys)
sys.setdefaultencoding('utf-8')



class SpringbreakSpider(scrapy.Spider):
    name = "springbreak"
    start_urls = [
        'http://www.shopspringbreak.com/',
        
    ]

    def parse(self, response):
        category = ['Shirts' , 'T-Shirts' , 'Joggers & Jeans' , 'Shorts' , 'Dresses & Jumpsuits', 'Tops', 'Jackets & Blazers', 'Bottom Wear', 'Jewellery', 'Co-Ords']
        for parentCategory in response.css('li.level0'):
            parentCategoryName = parentCategory.css('a.level-top span::text').extract_first().strip()
            parentCategoryLink = parentCategory.css('a.level-top::attr(href)').extract_first()
            #print parentCategoryName
            for subCategory in parentCategory.css('ul.level0 li.level1 a'):
                subCategoryName = subCategory.css('span::text').extract_first().strip()
                subCategoryLink = subCategory.css('::attr(href)').extract_first()
                if subCategoryName in category:
                    yield scrapy.Request(subCategoryLink, callback=self.parse_listing, meta={'subCategory':subCategoryName,'parentCategory':parentCategoryName})
    
    def parse_listing(self,response):
        metaData = response.meta
        for listing in response.css('li.item div.wrapper-hover'):
            listingLink = listing.css('a.product-image::attr(href)').extract_first()
            listingName = listing.css('a.product-image::attr(content)').extract_first()
            yield scrapy.Request(listingLink, callback=self.productDetail, meta={'listingLink':listingLink, 'subCategory':metaData['subCategory'], 'parentCategory':metaData['parentCategory']})

        next_page = response.css('div.pages ol li a.next::attr(href)').extract_first()
        if next_page is not None:
            next_page = response.urljoin(next_page)
            yield scrapy.Request(next_page, callback=self.parse_listing, meta={'subCategory':metaData['subCategory'], 'parentCategory':metaData['parentCategory']})    

    def productDetail(self,response):
        store = "585b78c587feb105133c986b"
        metadata = response.meta
        for product in response.css('div.col-main'):
            now = datetime.datetime.now()
            price = product.css('div.price-box p.special-price span.price::text')
            if price:
                actual_price = re.sub('\xe2\x82\xb9','',''.join(str(e) for e in map(unicode.strip, product.css('div.price-box p.old-price span.price::text').extract_first())))
                discounted_price = re.sub('\xe2\x82\xb9','',''.join(str(e) for e in map(unicode.strip, product.css('div.price-box p.special-price span.price::text').extract_first())))
            else:
                actual_price = re.sub('\xe2\x82\xb9','',''.join(str(e) for e in map(unicode.strip, product.css('div.product-options-bottom div.price-box span.regular-price span.price::text').extract_first())))
                discounted_price = re.sub('\xe2\x82\xb9','',''.join(str(e) for e in map(unicode.strip, product.css('div.product-options-bottom div.price-box span.regular-price span.price::text').extract_first())))
            
            yield{
                'url':metadata['listingLink'],
                'images':product.css('div.container-slider ul.slider li a.cloud-zoom-gallery::attr(href)').extract(),
                'name':re.sub('\u2022','',''.join(str(e) for e in map(unicode.strip, product.css('div.product-shop div.product-name h1::text').extract()))),
                'subcategory':metadata['subCategory'],
                'category':metadata['parentCategory'],
                'actual_price':actual_price,
                'discounted_price':discounted_price,
                'store_id':store,
                'description':re.sub('\r\n','',''.join(str(e) for e in map(unicode.strip, product.css('div.easytabs-content div.std').extract()))),
                'gender':metadata['parentCategory'],
                'created_at':now.strftime("%Y-%m-%d %H:%M:%S")



            }


