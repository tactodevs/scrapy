import scrapy
import re
import datetime
import codecs
import sys
import json
reload(sys)
sys.setdefaultencoding('utf-8')



class AzureSpider(scrapy.Spider):
    name = "azure"
    start_urls = [
        'http://www.azurefashion.com/',
        
    ]

    def parse(self, response):
        for category in response.css('div#inner_container div#nav ul li#product-link a'):
            categoryName = category.css('::text').extract_first()
            categoryLink = category.css('::attr(href)').extract_first()
            yield scrapy.Request("http://www.azurefashion.com"+categoryLink, callback=self.parse_listing , meta={'categoryName':categoryName,'categoryLink':categoryLink})

    def parse_listing(self, response):
        metadata = response.meta
        #print metadata['categoryLink']
        status = 1 if (len(response.css('div#products_list_container div.products_list div.products_list_item')) and response.status == 200) else 0
        if status:

            for listing in response.css('div#products_list_container div.products_list div.products_list_item'):
                listingLink = listing.css('a::attr(href)').extract_first()
                yield scrapy.Request("http://www.azurefashion.com"+listingLink, callback=self.product , meta={'categoryName':metadata['categoryName'],'categoryLink':metadata['categoryLink'],'listingLink':listingLink})
                
            next_page = "http://www.azurefashion.com"+re.sub('\u2022','',''.join(str(e) for e in map(unicode.strip, response.css('li.next a::attr(href)').extract_first())))
            print next_page
            if next_page is not None:
                next_page = response.urljoin(next_page)
                yield scrapy.Request(next_page, callback=self.parse_listing, meta={'categoryName':metadata['categoryName'],'categoryLink':metadata['categoryLink']})
        else: 
            print "no data"    
    def product(self, response):
        meta = response.meta
        for products in response.css('div#content div#product-container'):
            now = datetime.datetime.now()
            link = meta['listingLink']
            data = link.split("/")
            scrappedImages = response.css('div#product-image-container div.product_thumb_img_container ul li a::attr(href)').extract()
            images=[]
            if scrappedImages:
                for image in scrappedImages:
                    images.append("http://www.azurefashion.com"+image)
            yield{
                'name':products.css('div#product-description-container h2::text').extract_first(),
                'description':re.sub('\u2022','',''.join(str(e) for e in map(unicode.strip, products.css('div.product-brief p::text').extract()))),
                'category':meta['categoryName'],
                'url':"http://www.azurefashion.com"+meta['listingLink'],
                'gender':"female",
                'store_id':"597c264770adb9862baaa16a",
                'images':images,
                'actual_price':re.sub('\u2022','',''.join(str(e) for e in map(unicode.strip, response.css('input#order_price::attr(value)').extract()))),
                'discounted_price':re.sub('\u2022','',''.join(str(e) for e in map(unicode.strip, response.css('input#order_price::attr(value)').extract()))),
                'sku':data[3],
                'created_at':now.strftime("%Y-%m-%d %H:%M:%S")

            }
