import scrapy
import re
import demjson


class CommonItem(scrapy.Item):
    brand = scrapy.Field()
    name = scrapy.Field()
    parent_category = scrapy.Field()
    sub_category = scrapy.Field()
    color = scrapy.Field()
    color_hex = scrapy.Field()
    color_variants = scrapy.Field()
    description = scrapy.Field()
    images = scrapy.Field()
    price = scrapy.Field()
    selling_price = scrapy.Field()
    sizes = scrapy.Field()
    inventory = scrapy.Field()
    sku = scrapy.Field()
    url = scrapy.Field()
    style = scrapy.Field()
    color_hex_images_urls = scrapy.Field()
    color_hex_images = scrapy.Field()


class SharmilSpider(scrapy.spiders.Spider):
    name = "next_direct_spider"
    allowed_domains = ['nextdirect.com']
    count = 0
    brands = []
    start_urls = [

            'http://www.nextdirect.com/in/en/brands/all',
            'http://www.nextdirect.com/in/en/menu/meganav/men?contentonly=true&parentLinkID=02-02'
            '&parentLinkName=Men&v=0.9.14.19120'
            'http://www.nextdirect.com/in/en/menu/meganav/women?contentonly=true&parentLinkID=02-02'
            '&parentLinkName=Men&v=0.9.14.19120' ]

    def parse(self, response):
        for brand in response.xpath('//section[@class="BrandsListing"]/div/ul/li/a/text()').extract():
            self.brands.append(brand)

        category = ''
        if '/men?' in response.url and 'LinkName=Men' in response.url:
            category = 'Men'
        if '/women?' in response.url and 'LinkName=Women' in response.url:
            category = 'Women'
        for cat in response.xpath('//li[@class="ComponentItem"]/div/a/@href').extract():
            if '-newin' in cat:
                continue
            yield scrapy.Request(cat, meta={'category': category}, callback=self.parse_pages)

    def parse_pages(self, response):
        subcat = response.xpath('//*[@id="ResultHeader"]/div/h1/text()').extract_first()

        for product in response.xpath('//article[contains(@class, "Item")]/section/div[@class="Info"]'
                                      '/h2/a/@href').extract():

            yield scrapy.Request(product, meta={'category': response.meta['category'], 'sub_category': subcat},
                                 callback=self.parse_item)

            next_page = response.xpath('//a[@class="Page Next"]/@href').extract_first()
            if next_page:
                next_page = 'http://www.nextdirect.com/in/en' + next_page.split('http://www.nextdirect.com')[1]
                yield scrapy.Request(next_page, meta={'category': response.meta['category']}, callback=self.parse_pages)

    def parse_item(self, response):
        json_data = re.search('var shotData.*;<', response.body).group()[15:-2]
        json_data = demjson.decode(json_data)
        brand_item = None
        for brand in self.brands:
            if brand in response.xpath('//section[@class="ProductDetail"]/article/section[@class="StyleCopy"]'
                                       '/div[@class="StyleHeader"]/div/h1/text()').extract_first():
                brand_item = brand

        colors = [color['Colour'] for color in json_data['Styles'][0]['Fits'][0]['Items']]
        for style_item in json_data['Styles'][0]['Fits']:
            style = style_item['Name']

            for idx, item_color in enumerate(style_item['Items']):
                item = CommonItem()
                item['url'] = response.url
                item['brand'] = brand_item
                acrticle = response.xpath('//section[@class="ProductDetail"]/article')[0]
                item['name'] = acrticle.xpath('section[@class="StyleCopy"]/div[@class="StyleHeader"]/div/h1/text()')\
                    .extract_first()
                item['price'] = item_color['FullPrice']
                item['selling_price'] = acrticle.xpath('section[@class="StyleCopy"]/div[@class="Price"]/span/text()')\
                    .extract_first()

                item['parent_category'] = response.meta['category']
                item['sub_category'] = response.meta['sub_category']

                desc = []
                for i in response.xpath('//*[@id="ToneOfVoice"]/following-sibling::div/text()').extract() + \
                    response.xpath('//*[@id="ToneOfVoice"]/text()').extract():
                    i = i.encode('ascii', 'ignore').strip()
                    if i == '':
                        continue
                    desc.append(i)
                desc = str(map(str, desc))
                desc = desc.replace('[', '').replace(']', '').replace('\'', '\"')
                item['description'] = desc

                item['sku'] = item_color['ItemNumber']

                item['color'] = item_color['Colour']
                item['color_hex_images_urls'] = [item_color['Image']]
                item['color_variants'] = colors                
                item['sizes'] = [size['Name'] for size in item_color['Options']]

                selected_color = response.xpath('//select[@name="Colour"]/option[@selected="selected"]/text()')\
                    .extract_first()
                img_detail1 = response.xpath('//*[@id="hdnPage"]/@value').extract_first()
                img_detail2 = response.xpath('//*[@id="hdnPublication"]/@value').extract_first()
                img_path = json_data['ShotBasePath'] + img_detail2 + json_data['ShotZoomPath'] + img_detail1 + '/'
                imgs = []
                color_img = 'http://cdn.next.co.uk/Common/Items/Default/Default/ItemImages/AltItemZoom/' + \
                            item_color['Image'].split('/')[-1]               
                imgs.append(color_img)
                
                if selected_color == item['color'] or item['color'] == '':
                    if 'Media' in json_data.keys():
                        for img in json_data['Media']:
                            imgs.append(img_path + img['name'] + '.jpg')                
                item['images'] = imgs

                inventory = item_color['Options']
                for s in inventory:
                    if 'InStock' in s['StockStatus']:
                        s['quantity'] = 1
                    else:
                        s['quantity'] = 0
                    s['sku'] = s['Name']
                    s['price'] = s['selling_price'] = s['Price']
                    s['url'] = response.url
                    s.pop('StockStatus', None)
                    s.pop('Name', None)
                    s.pop('Number', None)
                    s.pop('Price', None)
                item['inventory'] = inventory

                item['style'] = style

                yield item