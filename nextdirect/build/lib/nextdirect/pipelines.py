# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import os
import scrapy
from scrapy.pipelines.images import ImagesPipeline
from PIL import Image
import pymongo
import datetime


class ColorHexPipeline(ImagesPipeline):

    def get_media_requests(self, item, info):
        for image_url in item['color_hex_images_urls']:
            yield scrapy.Request(image_url)

    def item_completed(self, results, item, info):
        image_paths = [x['path'] for ok, x in results if ok]
        if image_paths != [] or item['color'] != '':
            try:
                image_path = os.getcwd() + '/colors/' + image_paths[0]
                im = Image.open(image_path)
                pix = im.load()
                color_hex = '#%02x%02x%02x' % pix[1, 1]
                item['color_hex'] = color_hex
                # os.remove(image_path)
            except:
                pass
        else:
            item['color_hex'] = None
        item['color_hex_images_urls'] = image_paths
        return item

    def close_spider(self, spider):
        import shutil
        shutil.rmtree(os.getcwd() + '/colors/')


user = 'upwork'
password = 'upW0rk123'
host = '52.76.88.187'
db_name = 'scrapy'

uri = "mongodb://%s:%s@%s/%s" % (user, password, host, db_name)
client = pymongo.MongoClient(uri)
db = client[db_name]
collection = db['nextdirect']


class ExportToMongoDB(object):
    def process_item(self, item, spider):
        item_dict = dict(item)
        exist = collection.update_one({'sku': item_dict['sku'],
                                       'url': item_dict['url'],
                                       'color': item_dict['color'],
                                       'style': item_dict['style']}, {'$set': {'modified': datetime.datetime.now()}})
        if exist.matched_count == 0:
            item_dict['created'] = datetime.datetime.now()
            item_dict['modified'] = None
            collection.insert_one(item_dict)

        return item

    def close_spider(self, spider):
        client.close()