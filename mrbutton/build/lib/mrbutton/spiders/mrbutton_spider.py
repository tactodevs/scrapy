import scrapy
import re
import codecs
import sys
import json
reload(sys)
import datetime
sys.setdefaultencoding('utf-8')


class MrbuttonSpider(scrapy.Spider):
    name = "mrbutton"
    start_urls = [
        'https://www.mrbutton.in/',
        
    ]
    def parse(self, response):
        categories = ['SHIRTS' , 'T-SHIRTS' , 'TUNICS' , 'Trousers' , 'SHORTS' , 'JOGGERS' , 'BLAZERS' , 'NEHRU JACKETS' , 'WAISTCOATS' , 'JACKETS' , 'SWEATSHIRTS' , 'BAGS'  ]
        for category in response.css('div.header_mainbarcontainer div#nav-region aside#execphp-8 div.execphpwidget nav.main-navigation ul li'):
            categoryName = category.css('a::text').extract_first()
            categoryLink = category.css('a::attr(href)').extract_first()
            if categoryName in categories:
                yield scrapy.Request("https://www.mrbutton.in"+categoryLink,method="GET", callback=self.parseListing, meta={'categoryName':categoryName,'categoryLink':categoryLink, 'page':2})    
    
    def parseListing(self, response):
        metadata = response.meta
        status = 1 if len(response.css('div.imagecontentblock div.titleblock')) else 0
        if(status):
            for listing in response.css('div.imagecontentblock div.titleblock'):
                listingLink = listing.css('a::attr(href)').extract_first()
                #print listingLink
                yield scrapy.Request(listingLink,callback=self.product,method="GET", meta={'categoryName':metadata['categoryName'], 'listingLink':listingLink} )
            
            nextUrl = ('https://www.mrbutton.in'+metadata['categoryLink']+'page/'+str(metadata['page']))
            yield scrapy.Request(nextUrl,callback=self.parseListing,method="GET", meta={'categoryLink':metadata['categoryLink'], 'categoryName':metadata['categoryName'],'url':listingLink,'page':(metadata['page']+1)})  
        else:
            print 'No Data'          
    
    def product(self,response):
        metaData = response.meta
        for products in response.css('div.detailmain'):
                color=features=material='' 
                for moreInfo in response.css('div.detail_attributeblock'):
                    if (moreInfo.css('h2.detail_attributetitle::text').extract_first().strip() == 'Color'):
                        color = moreInfo.css('div.detail_attribute_content::text').extract_first().strip()
                    elif (moreInfo.css('h2.detail_attributetitle::text').extract_first().strip() == 'FABRIC'):
                        material = moreInfo.css('div.detail_attribute_content::text').extract_first().strip()
                    else:
                        print 'Not Match'

                if (products.css('div.price del span.woocommerce-Price-amount').extract_first()):
                    actual_price = re.sub('\u2022','',''.join(str(e) for e in map(unicode.strip, products.css('div.price del span.woocommerce-Price-amount::text').extract_first())))
                    discounted_price = re.sub('\u2022','',''.join(str(e) for e in map(unicode.strip, products.css('meta::attr(content)').extract_first())))
                else:
                    actual_price = re.sub('\u2022','',''.join(str(e) for e in map(unicode.strip, products.css('meta::attr(content)').extract_first())))
                    discounted_price = re.sub('\u2022','',''.join(str(e) for e in map(unicode.strip, products.css('meta::attr(content)').extract_first())))
                now = datetime.datetime.now()
                size = map(unicode.strip, products.css('div.variations_container div.value select option::text').extract())
                del size[0]
                yield{
                    'name':re.sub('\u2022','',''.join(str(e) for e in map(unicode.strip, products.css('div.detail_rightblock div.entry-summary h1::text').extract()))),
                    'category':metaData['categoryName'],
                    'url':metaData['listingLink'],
                    'gender':"male",
                    'store_id':"58f6f94570adb9c77dacca05",
                    'sku':re.sub('\u2022','',''.join(str(e) for e in map(unicode.strip, products.css('span.sku_wrapper span.sku::text').extract()))),
                    'description':re.sub('\u2022','',''.join(str(e) for e in map(unicode.strip, products.css('div.product-description p::text').extract()))),
                    'size':size,
                    'images':map(unicode.strip, products.css('div.images ul.bxslider_detail li a::attr(href)').extract()),
                    'color':color,
                    'material':material,
                    'actual_price':actual_price,
                    'discounted_price':discounted_price,
                    'created_at':now.strftime("%Y-%m-%d %H:%M:%S")
                }
                
