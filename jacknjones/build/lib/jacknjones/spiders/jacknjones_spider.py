import scrapy
import re
import codecs
import sys
import json
reload(sys)
import datetime
sys.setdefaultencoding('utf-8')


class JacknjonesSpider(scrapy.Spider):
    name = "jacknjones"
    start_urls = [
        'http://www.jackjones.in'
    ]
    def parse(self, response):
        for superparent in response.css('ul.shop-by-links li.accordion-toggle'):
            superparentname =  superparent.css('a::text').extract_first().strip()
            superparentlink = superparent.css('a::attr(href)').extract_first()
            if (superparentname != 'Promotions'):
                for childCategory in superparent.css('ul.sub_category_links li'):
                    childCategoryName = childCategory.css('a::text').extract_first()
                    childCategoryLink = childCategory.css('a::attr(href)').extract_first()
                    yield scrapy.Request(childCategoryLink,callback=self.parse_listing,method="GET", meta={'link':childCategoryLink, 'categoryName':childCategoryName, 'parentcategoryName':superparentname, 'page':2})
    
    
    def parse_listing(self, response):
        metaData = response.meta
        status = 1 if len(response.css('a.product-image')) else 0
        if(status):
            for listing in response.css('a.product-image'):
                listingLink = listing.css('::attr(href)').extract_first()
                #print listingLink
                yield scrapy.Request(listingLink,callback=self.productDetail,method="GET", meta={'categoryName':metaData['categoryName'], 'parentcategoryName':metaData['parentcategoryName'], 'url':listingLink} )
            
            nextUrl = (metaData['link']+'?dir=desc&order=discount_percent&is_ajax=1&p='+str(metaData['page'])+'&is_scroll=1')
            yield scrapy.Request(nextUrl,callback=self.parse_listing,method="GET", meta={'link':metaData['link'], 'categoryName':metaData['categoryName'], 'parentcategoryName':metaData['parentcategoryName'],'url':listingLink,'page':(metaData['page']+1)})  
        else:
            print 'No Data'          



    def productDetail(self, response):
        metadaata = response.meta
        store = "5846576487feb1d70b3c9869"
        for jackjones in response.css('div.main'):
              var = map(unicode, jackjones.css('div.price-box span.regular-price span.price::text'))
              if var: 
                price =  re.sub('Rs.','',''.join(str(e) for e in map(unicode.strip, jackjones.css('div.price-box span.regular-price span.price::text').extract_first())))
                discounted_price = re.sub('Rs.','',''.join(str(e) for e in map(unicode.strip, jackjones.css('div.price-box span.regular-price span.price::text').extract_first())))
              else:
                price = re.sub('Rs.','',''.join(str(e) for e in map(unicode.strip, jackjones.css('div.price-box p.price-old-price-reg span.price::text').extract_first())))
                discounted_price = re.sub('Rs.','',''.join(str(e) for e in map(unicode.strip, jackjones.css('div.price-box p.special-price span.price::text').extract_first())))
              now = datetime.datetime.now()
              sku=color=pattern=occassion=material='' 
              for moreInfo in response.css('div.product-attr-pan'):
                    if (moreInfo.css('div.attr-lable::text').extract_first().strip() == 'Sku'):
                        sku = moreInfo.css('div.attr-value::text').extract_first().strip()
                    elif (moreInfo.css('div.attr-lable::text').extract_first().strip() == 'Colour'):
                        color = moreInfo.css('div.attr-value::text').extract_first().strip()
                    elif (moreInfo.css('div.attr-lable::text').extract_first().strip() == 'Fabric'):
                        material = moreInfo.css('div.attr-value::text').extract_first().strip()
                    elif (moreInfo.css('div.attr-lable::text').extract_first().strip() == 'Pattern'):
                        pattern = moreInfo.css('div.attr-value::text').extract_first().strip()
                    elif (moreInfo.css('div.attr-lable::text').extract_first().strip() == 'Occassion'):
                        occassion = moreInfo.css('div.attr-value::text').extract_first().strip()
                    else:
                        print 'Not Match'
              yield {                
                    'name': re.sub('\u2022','',''.join(str(e) for e in map(unicode.strip, jackjones.css('div.product-view div.tablet_view div.product-name h1::text').extract()))),
                    'images' : map(unicode.strip, response.css('div.product-img-box div.zoom div.image-slider li img::attr(src)').extract()),
                    'description':re.sub('\u2022','',''.join(str(e) for e in map(unicode.strip, jackjones.css('div.desc-content div.box-lt::text').extract()))),
                    'discounted_price':discounted_price,
                    'actual_price':price,
                    'subcategory':metadaata['categoryName'],
                    'category':metadaata['parentcategoryName'],
                    'sku':sku,
                    'color':color,
                    'pattern':pattern,
                    'occassion':occassion,
                    'material':material,
                    'sizes':map(unicode.strip, jackjones.css('dd.last span.size-varients::text').extract()),
                    'store_id':store,
                    'url':metadaata['url'],
                    'created_at':now.strftime("%Y-%m-%d %H:%M:%S"),
                    'gender':"male"

                    }      
              
              