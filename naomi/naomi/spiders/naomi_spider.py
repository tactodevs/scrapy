import scrapy
import re
import codecs
import sys
import json
import datetime
reload(sys)
sys.setdefaultencoding('utf-8')

class NaomiSpider(scrapy.Spider):
    name = "naomi"
    start_urls = [
        'http://www.naomicode.com/',
    ]

    def parse(self, response):
        categories = ['Tops', 'Bottoms' , 'Jumpsuits & Coordinates', 'Dresses' ,'Denim' , 'Outerwear']
        for category in response.css('li.col1'):
            categoryLink = category.css('a::attr(href)').extract_first()
            categoryName = category.css('a::text').extract_first()
            if categoryName in categories:
                yield scrapy.Request(categoryLink, callback=self.parseListing, meta={'categoryName':categoryName , 'categoryLink':categoryLink, 'page':2})

    def parseListing(self, response):
        metadata = response.meta
        
        status = 1 if len(response.css('div.grid_holder div.col-lg-4 div.proCategoryMrg2 div.product-block div.figcaption-wrapper h2.name a')) else 0
        if(status):
            for listing in response.css('div.grid_holder div.col-lg-4 div.proCategoryMrg2 div.product-block div.figcaption-wrapper h2.name a'):
                listingLink = listing.css('::attr(href)').extract_first()
                ListingName = listing.css('::text').extract_first()
                yield scrapy.Request(listingLink,callback=self.product,meta={'categoryName':metadata['categoryName'],'url':listingLink} )

            nextUrl = (metadata['categoryLink']+'&page='+str(metadata['page']))
            yield scrapy.Request(nextUrl,callback=self.parseListing, meta={'categoryLink':metadata['categoryLink'], 'categoryName':metadata['categoryName'],'url':listingLink,'page':(metadata['page']+1)})  
        else:
            print 'No Data'

    def product(self, response):
        meta = response.meta
        store_id = "5979e36f70adb9d019b72e56"
        for products in response.css('div.product div.product-info'):
            sku=brand=''
            for productdetail in response.css('div.description ul li'):
                if (productdetail.css('span.contrast_font::text').extract_first() == 'Product Code:'):
                    sku = re.sub('\u2022','',''.join(str(e) for e in map(unicode.strip, productdetail.css('li::text').extract_first())))
                elif(productdetail.css('span.contrast_font::text').extract_first() == 'Brand:'):
                    brand = re.sub('\u2022','',''.join(str(e) for e in map(unicode.strip, productdetail.css('li a::text').extract_first())))
                else:
                    'Not Match'
            now = datetime.datetime.now()
            sizes = map(unicode.strip, products.css('div.wdspan2 div.options div.option div select.small option::text').extract())
            del sizes[0]
            del sizes[0]
            yield{
                'images':map(unicode.strip, products.css('div.image div.MagicToolboxContainer div.MagicToolboxSelectorsContainer a::attr(href)').extract()),
                'category':meta['categoryName'],
                'name':re.sub('\u2022','',''.join(str(e) for e in map(unicode.strip, products.css('div.right h1::text').extract()))), 
                'url':meta['url'],
                'store_id':store_id,
                'discounted_price':re.sub('Rs. ','',''.join(str(e) for e in map(unicode.strip, products.css('div.extended_offer div.price-new span.amount::text').extract()))),
                'actual_price':re.sub('Rs. ','',''.join(str(e) for e in map(unicode.strip, products.css('div.extended_offer div.price-old span.amount del::text').extract()))),
                'sizes':sizes,
                'brand':brand,
                'sku':sku,
                'gender':"female",
                'description':re.sub('\u2022','',''.join(str(e) for e in map(unicode.strip, products.css('div.description ul li p span::text').extract()))),
                'created_at':now.strftime("%Y-%m-%d %H:%M:%S")
            }