import scrapy
import re
import codecs
import sys
import json
reload(sys)
import datetime
sys.setdefaultencoding('utf-8')


class LabellifeSpider(scrapy.Spider):
    handle_httpstatus_list = [400]
    name = "labellife"
    start_urls = [
        'https://www.thelabellife.com/',
        
    ]

    def parse(self, response):
        subcategories = ['Dresses' , 'Tops' , 'Skirts and Bottoms' , 'Swimwear', 'Nightwear' , 'Flats' , 'Heels And Wedges' , 'Brogues' , 'Jewellery' , 'BAGS' , 'Add Ons' ]
        categories = ['CLOTHING' , 'SHOES' , 'ACCESSORIES']
        for category in response.css('li.master_li'):
            categoryName = category.css('a::text').extract_first()
            categoryLink = category.css('::attr(href)').extract_first()
            if categoryName in categories:
                for subcategory in category.css('div.level2 ul li'):
                    subcategoryName = subcategory.css('a::text').extract_first()
                    subcategoryLink = subcategory.css('a::attr(href)').extract_first()
                    if subcategoryName in subcategories:
                        yield scrapy.Request(subcategoryLink, callback=self.parse_listing , meta={'categoryName':categoryName , 'subcategoryName':subcategoryName})

    def parse_listing(self, response):
        metadata = response.meta
        for listing in response.css('div.amshopby-page-container div.outer-div-list div.item div.pro_topadst div.pro_list_img'):
            listingName = listing.css('a.product-image::attr(title)').extract_first()
            listingLink = listing.css('a.product-image::attr(href)').extract_first()
            yield scrapy.Request(listingLink, callback=self.productdetail, meta={'categoryName':metadata['categoryName'], 'subcategoryName':metadata['subcategoryName'], 'listingLink':listingLink})

    def productdetail(self, response):
        metaData = response.meta 
        for products in response.css('div.product-view div.product-essential'):
            now = datetime.datetime.now()
            var = products.css('div.product-shop div div.price-box span.regular-price')
            if var: 
                discounted_price = re.sub('\xe2\x82\xb9','',''.join(str(e) for e in map(unicode.strip, products.css('div.product-shop div div.price-box span.regular-price spanclass::text').extract())))
                actual_price = re.sub('\xe2\x82\xb9','',''.join(str(e) for e in map(unicode.strip, products.css('div.product-shop div div.price-box span.regular-price spanclass::text').extract())))
            
            else:
                discounted_price = re.sub('\xe2\x82\xb9','',''.join(str(e) for e in map(unicode.strip, products.css('div.price-box p.special-price span.price::text').extract())))
                actual_price = re.sub('\xe2\x82\xb9','',''.join(str(e) for e in map(unicode.strip, products.css('div.price-box p.old-price span.price::text').extract())))      
            yield{
                'category':metaData['categoryName'],
                'subcategory':metaData['subcategoryName'],
                'gender':"female",
                'name':re.sub('\u2022','',''.join(str(e) for e in map(unicode.strip, products.css('div.product-shop div div.product-name h1::text').extract()))),
                'images': map(unicode.strip, products.css('div.product-img-box div.pdp_img_wrap div.product_main_img a.fresco::attr(href)').extract()),
                'url':metaData['listingLink'],
                'description':re.sub('\r\n','',''.join(str(e) for e in map(unicode.strip, products.css('div#wrapperAccordion_pdp div.accord_wrap div.accordion-content').extract_first()))),
                'discounted_price':discounted_price,
                'actual_price':actual_price,
                'store_id':"58fb90ac70adb9b263e79bc7",
                'created_at':now.strftime("%Y-%m-%d %H:%M:%S"),
            }